package vault

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	hProtocol "github.com/stellar/go/protocols/horizon"
)

//Client is the client to the Vault api, create a new client with NewClient
type Client struct {
	*api.Client
}

//NewClient returns a client configured to access vault,
//address should come from environment variable VAULT_ADDR and token should come from VAULT_TOKEN
func NewClient(httpClient *http.Client) (*Client, error) {

	envAddress := os.Getenv(api.EnvVaultAddress)

	client, err := api.NewClient(&api.Config{Address: envAddress, HttpClient: httpClient})
	return &Client{client}, err
}

//UserpassLogin - log into vault using the userpass mount returns token
func (c *Client) UserpassLogin(username, password string) (string, error) {
	if username == "" {
		return "", errors.New("Username is required")
	}

	if password == "" {
		return "", errors.New("Password is required")
	}

	token := os.Getenv(api.EnvVaultToken)
	c.SetToken(token)

	userexists, err := c.Logical().Read(fmt.Sprintf("auth/userpass/users/%s", username))
	if err != nil {
		return "", err
	}

	if userexists == nil {
		return "", errors.New("User Does Not Exist")
	}

	//have to clear the token for userpass auth
	c.ClearToken()

	secret, err := c.Logical().Write(fmt.Sprintf("auth/userpass/login/%s", username), map[string]interface{}{
		"password": password,
	})
	if err != nil {
		return "", err
	}

	token, err = secret.TokenID()

	return token, err
}

//RevokeToken revokes token and all of its children
func (c *Client) RevokeToken(token string) (bool, error) {

	envtoken := os.Getenv(api.EnvVaultToken)
	c.SetToken(envtoken)

	_, err := c.Logical().Write("/auth/token/revoke", map[string]interface{}{
		"token": token,
	})
	if err != nil {
		return false, err
	}

	return true, nil
}

//RevokeOrphanToken revokes token but not its children
func (c *Client) RevokeOrphanToken(token string) (bool, error) {

	envtoken := os.Getenv(api.EnvVaultToken)
	c.SetToken(envtoken)

	_, err := c.Logical().Write("/auth/token/revoke-orphan", map[string]interface{}{
		"token": token,
	})
	if err != nil {
		return false, err
	}

	return true, nil
}

//RevokeAllLeases revokes all leases associated with a userpass login in vault
func (c *Client) RevokeAllLeases(username string) (bool, error) {
	if username == "" {
		return false, errors.New("Username is missing")
	}

	envtoken := os.Getenv(api.EnvVaultToken)
	c.SetToken(envtoken)

	_, err := c.Logical().Write(fmt.Sprintf("sys/leases/revoke-prefix/auth/userpass/login/%s", username), map[string]interface{}{})
	if err != nil {
		return false, err
	}

	c.ClearToken()

	return true, nil
}

//LookupToken returns the information associated with the token
func (c *Client) LookupToken(token string) (*api.Secret, error) {
	if token == "" {
		return nil, errors.New("LookupToken: Token missing")
	}

	envtoken := os.Getenv(api.EnvVaultToken)
	c.SetToken(envtoken)

	//@todo figure out how to use lookup-self instead here, it breaks on the jwt tokenid
	secret, err := c.Logical().Write("auth/token/lookup", map[string]interface{}{
		"token": token,
	})

	c.ClearToken()

	return secret, err
}

//RenewToken renews a token using the service token
func (c *Client) RenewToken(token, increment string) (*api.Secret, error) {
	if token == "" {
		return nil, errors.New("RenewToken: Token missing")
	}

	envtoken := os.Getenv(api.EnvVaultToken)
	c.SetToken(envtoken)

	//@todo figure out how to use lookup-self instead here, it breaks on the jwt tokenid
	secret, err := c.Logical().Write("auth/token/renew", map[string]interface{}{
		"token":     token,
		"increment": increment,
	})

	c.ClearToken()

	return secret, err
}

//RenewSelf renews token using the token itself
func (c *Client) RenewSelf(token, increment string) (*api.Secret, error) {
	if token == "" {
		return nil, errors.New("RenewSelf: Token missing")
	}

	// envtoken := os.Getenv(api.EnvVaultToken)
	c.SetToken(token)

	//@todo figure out how to use lookup-self instead here, it breaks on the jwt tokenid
	secret, err := c.Logical().Write("auth/token/renew-self", map[string]interface{}{
		"increment": increment,
	})

	c.ClearToken()

	return secret, err
}

//GetJwt - get a JWT secret from vault using a userpass token, secret is important for client_id https://www.vaultproject.io/api-docs/secret/identity/tokens#sample-response-6
func (c *Client) GetJwt(token, role string) (*api.Secret, error) {
	if token == "" {
		return nil, errors.New("GetJwt: Token missing")
	}

	if role == "" {
		return nil, errors.New("GetJwt: Role missing")
	}

	c.SetToken(token)

	secret, err := c.Logical().Read(fmt.Sprintf("identity/oidc/token/%s", role))

	c.ClearToken()

	return secret, err
}

//JwtLogin uses a jwt to authenticate and return a jwt authenticated secret
func (c *Client) JwtLogin(jwt string, role string) (*api.Secret, error) {
	if jwt == "" {
		return nil, errors.New("JwtAuth: Jwt missing")
	}

	c.ClearToken()

	secret, err := c.Logical().Write("/auth/jwt/login", map[string]interface{}{
		"jwt":  jwt,
		"role": role,
	})

	return secret, err
}

//SetMetadata will update an entity's meta data
func (c *Client) SetMetadata(token, entityid string, metadata map[string]string) (*api.Secret, error) {
	c.SetToken(token)

	//https://www.vaultproject.io/api-docs/secret/identity/entity#update-entity-by-id
	secret, err := c.Logical().Write(fmt.Sprintf("identity/entity/id/%s", entityid), map[string]interface{}{
		"metadata": metadata,
	})
	c.ClearToken()

	return secret, err
}

//CreateSecret creates a new secret in vault
func (c *Client) CreateSecret(token, entityID, name string, data interface{}, mountPath string) (*api.Secret, error) {

	if entityID == "" {
		return nil, errors.New("entityID missing")
	}

	if name == "" {
		return nil, errors.New("Alias name missing")
	}

	c.SetToken(token)

	path := fmt.Sprintf("%s/data/%s/keypairs/%s", mountPath, entityID, name)

	exists, err := c.Logical().Read(path)
	if err != nil {
		//this will most likely indicate no records found which is what we wanted so we can create a new record so no need to return here
		log.Infof("%s we can create a new one", err)
	}

	if exists != nil {
		return nil, errors.New("An account with that name already exists")
	}

	secret, err := c.Logical().Write(path, map[string]interface{}{
		"data": data,
	})

	c.ClearToken()

	return secret, err
}

//TokenData data used to get a new token
type TokenData struct {
	ID              string            `json:"id"`
	Policies        []string          `json:"policies"`
	RoleName        string            `json:"role_name"`
	Meta            map[string]string `json:"meta"`
	NoParent        bool              `json:"no_parent"`
	NoDefaultPolicy bool              `json:"no_default_policy"`
	Lease           string            `json:"lease"`
	TTL             string            `json:"ttl"`
	Renewable       bool              `json:"renewable"`
	ExplicitMaxTTL  string            `json:"explicit_max_ttl"`
	DisplayName     string            `json:"display_name"`
	NumUses         int               `json:"num_uses"`
	Period          string            `json:"period"`
	Type            string            `json:"type"`
	EntityAlias     string            `json:"entity_alias"`
}

//GetNewToken will create a new token from the give token
func (c *Client) GetNewToken(token string, data TokenData) (*api.Secret, error) {
	c.SetToken(token)

	//https://www.vaultproject.io/api-docs/auth/token#parameters
	secret, err := c.Logical().Write("auth/token/create", map[string]interface{}{
		"id":                data.ID,
		"role_name":         data.RoleName,
		"policies":          data.Policies,
		"meta":              data.Meta,
		"no_parent":         data.NoParent,
		"no_default_policy": data.NoDefaultPolicy,
		"renewable":         data.Renewable,
		"lease":             data.Lease,
		"ttl":               data.TTL,
		"type":              data.Type,
		"explicit_max_ttl":  data.ExplicitMaxTTL,
		"display_name":      data.DisplayName,
		"num_uses":          data.NumUses,
		"period":            data.Period,
		"entity_alias":      data.EntityAlias,
	})
	c.ClearToken()

	return secret, err
}

//------------------------------ functions below dont actually use client, they are on the instance for convenience ---------------------------

//JwtGroups group metadata added to jwt token
type JwtGroups struct {
	Ids   []string `json:"ids"`
	Names []string `json:"names"`
}

//JwtClaims claims from jwt
type JwtClaims struct {
	Aud       string            `json:"aud"`
	Exp       int               `json:"exp"`
	Iat       int               `json:"iat"`
	Iss       string            `json:"iss"`
	Namespace string            `json:"namespace"`
	Nbf       int               `json:"nbf"`
	Sub       string            `json:"sub"`
	Metadata  map[string]string `json:"metadata"`
	Groups    JwtGroups         `json:"groups"`
	Username  string            `json:"username"`
}

//ParseJwt get the values from a vault Jwt
func (c *Client) ParseJwt(jwt string) (*JwtClaims, error) {
	result := strings.Split(jwt, ".")

	if len(result) < 3 {
		return nil, errors.New("Can not parse Invalid Jwt format")
	}

	sDec, err := base64.RawURLEncoding.DecodeString(result[1])
	if err != nil {
		return nil, err
	}

	var claims JwtClaims
	err = json.Unmarshal(sDec, &claims)
	if err != nil {
		return nil, err
	}

	return &claims, nil
}

//Introspection result from Vault
type Introspection struct {
	Active bool `json:"active"`
}

//IsJwtActive verifies whether the jwt is still active or not
func (c *Client) IsJwtActive(jwt string, clientID string, httpClient *http.Client) (bool, error) {
	if jwt == "" {
		return false, errors.New("<VaultService>.JwtIntrospection: jwt missing")
	}

	data := url.Values{"token": {jwt}, "client_id": {clientID}}

	envaddress := os.Getenv(api.EnvVaultAddress)

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/identity/oidc/introspect", envaddress), strings.NewReader(data.Encode()))
	if err != nil {
		return false, err
	}

	envtoken := os.Getenv(api.EnvVaultToken)

	req.Header.Add("X-Vault-Token", envtoken)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := httpClient.Do(req)
	if err != nil {
		return false, err
	}

	defer resp.Body.Close()

	var introspection Introspection

	err = json.NewDecoder(resp.Body).Decode(&introspection)
	if err != nil {
		return false, err
	}

	return introspection.Active, nil
}

//GetSecretField returns the data associated with the field or nothing
func (c *Client) GetSecretField(secret *api.Secret, field string) (string, error) {
	if secret == nil {
		return "", nil
	}

	if secret.Data == nil || secret.Data[field] == nil {
		return "", nil
	}

	data, ok := secret.Data[field].(string)
	if !ok {
		return "", fmt.Errorf("token found but in the wrong format")
	}

	return data, nil
}

//GetAuthToken returns the token id which can be used for authentication.
func (c *Client) GetAuthToken(secret *api.Secret) (string, error) {
	return secret.TokenID()
}

//GetEntityID returns the entity id from a secret
func (c *Client) GetEntityID(secret *api.Secret) string {
	return secret.Auth.EntityID
}

// Account represents a Stellar account.
type Account struct {
	Seed     string            `json:"seed,omitempty"`
	Address  string            `json:"address"`
	HAccount hProtocol.Account `json:"account"`
	Exists   bool              `json:"exists"`
}

//GetKeypairByAlias returns the keypair stored under the secret path stellar/data/<entityID>/keypairs/<alias>
func (c *Client) GetKeypairByAlias(token, entityID, alias, mountPath string) (Account, error) {
	if alias == "" {
		return Account{}, errors.New("Account Alias missing")
	}

	if mountPath == "" {
		mountPath = "stellar" // change to secret after all services updated.
	}

	path := fmt.Sprintf("%s/data/%s/keypairs/%s", mountPath, entityID, alias)

	c.SetToken(token)

	secret, err := c.Logical().Read(path)
	if err != nil {
		return Account{}, err
	}

	// convert map to json
	jsonString, _ := json.Marshal(secret.Data["data"])

	log.Debugf("VaultService>.GetKeypairByAlias jsonString %v", string(jsonString))

	// // convert json to struct
	hAccount := Account{}
	json.Unmarshal(jsonString, &hAccount)

	result := &hAccount
	r := *result
	c.ClearToken()

	return r, nil
}

//GetKeypairByAddress loops through all the accounts stored under the entityID path and finds the keypair by looking at the addresses
func (c *Client) GetKeypairByAddress(token, entityID, address, mountPath string) (Account, error) {
	if address == "" {
		return Account{}, errors.New("Account Address missing")
	}

	if mountPath == "" {
		mountPath = "stellar"
	}

	c.SetToken(token)

	path := fmt.Sprintf("%s/metadata/%s/keypairs", mountPath, entityID)

	secret, err := c.Logical().List(path)
	if err != nil {
		return Account{}, err
	}

	_, ok := secret.Data["keys"]
	if !ok {
		return Account{}, fmt.Errorf("couldn't find any accounts")
	}

	list, ok := secret.Data["keys"].([]interface{})
	if !ok {
		return Account{}, fmt.Errorf("unable to convert to the expected format")
	}

	secretArray := make(map[string]Account, len(list))

	for _, val := range list {
		c.SetToken(token)
		p, err := c.Logical().Read(fmt.Sprintf("%s/data/%s/keypairs/%s", mountPath, entityID, val.(string)))
		if err != nil {
			return Account{}, fmt.Errorf("unable to convert alias %v to string", val)
		}

		jsonString, _ := json.Marshal(p.Data["data"])
		keypair := Account{}
		json.Unmarshal(jsonString, &keypair)

		secretArray[keypair.Address] = keypair
	}

	c.ClearToken()

	return secretArray[address], nil
}

//GetAddresses returns addresses for an entityID stored in vault
func (c *Client) GetAddresses(token, entityID, mountPath string) (map[string]map[string]interface{}, error) {
	path := fmt.Sprintf("%s/metadata/%s/keypairs", mountPath, entityID)

	c.SetToken(token)

	secret, err := c.Logical().List(path)
	if err != nil {
		return nil, err
	}

	if secret == nil {
		return nil, fmt.Errorf("couldn't find any accounts")
	}
	_, ok := secret.Data["keys"]
	if !ok {
		return nil, fmt.Errorf("couldn't find any accounts")
	}

	list, ok := secret.Data["keys"].([]interface{})
	if !ok {
		return nil, fmt.Errorf("unable to convert to the expected format")
	}

	//make the return object
	secretArray := make(map[string]map[string]interface{}, len(list))

	for _, val := range list {
		c.SetToken(token)
		p, err := c.Logical().Read(fmt.Sprintf("%s/data/%s/keypairs/%s", mountPath, entityID, val.(string)))
		if err != nil {
			return nil, fmt.Errorf("unable to convert alias %v to string", val)
		}

		jsonString, _ := json.Marshal(p.Data["data"])
		keypair := Account{}
		json.Unmarshal(jsonString, &keypair)

		secretArray[val.(string)] = map[string]interface{}{
			"address": keypair.Address,
		}

	}

	group, err := c.Logical().Read(fmt.Sprintf("identity/group/id/%s", entityID))
	if err != nil {
		return nil, fmt.Errorf("unable to get group data")
	}
	// groupJSON, _ := json.Marshal()
	secretArray["__data"] = group.Data
	c.ClearToken()

	return secretArray, nil
}

//GetEntityAccounts returns Stellar account addresses for an entityID stored in vault
func (c *Client) GetEntityAccounts(token, entityID, mountPath string) (Entity, error) {

	if token == "" {
		log.Info("No token provided")
		return Entity{}, errors.New("Token is required")
	}

	if entityID == "" {
		log.Info("No entity ID provided")
		return Entity{}, errors.New("Entity ID is required")
	}

	if mountPath == "" {
		log.Info("No mountPath provided")
		return Entity{}, errors.New("mountPath is required")
	}

	c.SetToken(token)

	entityData, err := c.Logical().Read(fmt.Sprintf("identity/group/id/%s", entityID))
	if err != nil {
		return Entity{}, fmt.Errorf("unable to get group data")
	}

	if entityData == nil {
		return Entity{}, fmt.Errorf("unable to get group data")
	}

	entityJSON, _ := json.Marshal(entityData.Data)
	eData := EntityData{}
	json.Unmarshal(entityJSON, &eData)

	entity := Entity{
		EntityID: entityID,
		Data:     eData,
	}

	secret, err := c.Logical().List(fmt.Sprintf("%s/metadata/%s/keypairs", mountPath, entityID))
	if err != nil {
		log.Infof("There was a problem listing accounts %s", err)
	}

	if secret != nil {
		_, ok := secret.Data["keys"]
		if !ok {
			log.Infof("Could not find any account keys for entityID %s", entityID)
		}

		list, ok := secret.Data["keys"].([]interface{})
		if !ok {
			log.Infof("unable to convert to the expected format %s", entityID)
		}

		var accounts []EntityAccounts

		for _, val := range list {
			p, err := c.Logical().Read(fmt.Sprintf("%s/data/%s/keypairs/%s", mountPath, entityID, val.(string)))
			if err != nil {
				return Entity{}, fmt.Errorf("unable to convert alias %v to string", val)
			}

			jsonString, _ := json.Marshal(p.Data["data"])
			keypair := Account{}
			json.Unmarshal(jsonString, &keypair)

			acct := EntityAccounts{
				Alias:   val.(string),
				Address: keypair.Address,
			}
			accounts = append(accounts, acct)
		}

		entity.Accounts = accounts
	}

	c.ClearToken()

	return entity, nil
}

//Entity struct for GetAddresses
type Entity struct {
	EntityID string           `json:"entity_id"`
	Data     EntityData       `json:"data"`
	Accounts []EntityAccounts `json:"accounts"`
}

//EntityData for GetAddresses
type EntityData struct {
	// Alias           string            `json:"alias,omitempty"`
	CreationTime    string            `json:"creation_time,omitempty"`
	ID              string            `json:"id,omitempty"`
	LastUpdateTime  string            `json:"last_update_time,omitempty"`
	MemberEntityIDs []string          `json:"member_entity_ids,omitempty"`
	MemberGroupIDs  []string          `json:"member_group_ids,omitempty"`
	Metadata        map[string]string `json:"metadata,omitempty"`
	// ModifyIndex     int               `json:"modify_index,omitempty"`
	Name string `json:"name,omitempty"`
	// Policies        []string          `json:"policies,omitempty"`
	// Type            string            `json:"type,omitempty"`
}

//EntityAccounts for GetAddresses
type EntityAccounts struct {
	Alias   string `json:"alias"`
	Address string `json:"address"`
}

//GetMountPath returns the secret mount path based on a mode switch
func (c *Client) GetMountPath(mode string) string {
	switch mode {
	case "public":
		return "stellar"
	case "test":
		return "stellar-test"
	case "private":
		return "anara-stellar"
	case "private-test":
		return "anara-stellar-test"
	default:
		return "stellar-test"
	}
}
