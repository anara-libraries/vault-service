package vault

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
)

var httpClient = &http.Client{
	Timeout: 10 * time.Second,
}

func setUp() *Client {
	os.Setenv("VAULT_ADDR", "http://vault.default.svc.cluster.local:8200")
	os.Setenv("VAULT_TOKEN", "root")

	client, err := NewClient(httpClient)
	if err != nil {
		log.Fatal(err)
	}
	return client
}
func TestGetEntityAccounts(t *testing.T) {
	client := setUp()

	addresses, err := client.GetEntityAccounts("root", "0e8d124d-0539-795d-e521-ce822ae5c6ed", "stellar-test")
	if err != nil {
		log.Error(err)
	}
	jsn, _ := json.Marshal(addresses)

	fmt.Printf("%s", jsn)
	// t.Errorf("Hello() = %q, want %q", got, want)

}

// func TestGetAddresses(t *testing.T) {
// 	client := setUp()

// 	addresses, err := client.GetAddresses("root", "0357853a-25ad-ba92-0f28-77fcacf46561", "stellar-test")
// 	if err != nil {
// 		log.Error(err)
// 	}

// 	fmt.Println(addresses)
// 	// t.Errorf("Hello() = %q, want %q", got, want)

// }
